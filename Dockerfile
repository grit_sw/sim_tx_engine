# FROM alpine:3.7 as base
# Message producer
FROM alpine:3.7 as base

WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 postgresql-libs librdkafka-dev
RUN apk add --virtual .build-deps \
    gcc \
    musl-dev \
    postgresql-dev \
    python3-dev && \
    python3 -m venv venv && \
    venv/bin/pip install --upgrade pip && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY api api
COPY simulator simulator
COPY start.sh logger.py run.py stream.avsc ./

CMD chmod +x start.sh
ENTRYPOINT ["./start.sh"]


# Simulator API
FROM base as api
WORKDIR /home/user/
COPY sim_api.py start_api.sh ./
RUN chmod +x start_api.sh
ENTRYPOINT ["./start_api.sh"]


# Meter Order listener
FROM base as listener
WORKDIR /home/user/
COPY run_tx_listener.py start_listener.sh ./
RUN chmod +x start_listener.sh
ENTRYPOINT ["./start_listener.sh"]


# Electricity consumer meter
FROM base as consumer
WORKDIR /home/user/
COPY run_meter_consumer.py transaction.avsc stream.avsc start_consumer.sh ./
RUN chmod +x start_consumer.sh
ENTRYPOINT ["./start_consumer.sh"]


# Electricity producer meter
FROM base as producer
WORKDIR /home/user/
COPY run_meter_producer.py transaction.avsc stream.avsc start_producer.sh ./
RUN chmod +x start_producer.sh
ENTRYPOINT ["./start_producer.sh"]
