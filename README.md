# G1 Meter Readings Simulator

##Setting Up the simulator
1. Install [python3.6](https://askubuntu.com/a/865569) and [pipenv](https://docs.pipenv.org/)
1. Install librdkafka `sudo apt-get install librdkafka-dev`
1. Install the python dependencies `pipenv --python 3.6 install`
1. Activate your vitual environment `pipenv shell`
1. Start the simulator `python run.py`


