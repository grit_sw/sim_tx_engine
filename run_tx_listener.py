from simulator.consumer.consumer import create_consumer
from simulator.consumer.consumer import start_consumer

c = create_consumer()
print('Starting consumer')
start_consumer(c)
