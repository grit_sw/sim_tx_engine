"""
	ONLY RUN THIS IN A DEVELOPMENT ENVIRONMENT, AGAINST A DEVELOPMENT DATABASE.
	Script to setup dummy prosumers and energy plans to be used by the transaction engine for billing.
"""
import requests
from pprint import pprint
from time import sleep

base_url = 'http://localhost:5500'
prosumer_url = f'{base_url}/prosumers/'
energy_plan_url = f'{base_url}/energy_plans/all/'
new_energy_plan_url = f'{base_url}/energy_plans/new/'
login_url = f'{base_url}/auth/login/'
facility_url = f'{base_url}/facility/all/'
new_facility_url = f'{base_url}/facility/new/'
new_prosumer_url = f'{base_url}/prosumers/new/'
all_user_admins_url = f'{base_url}/users/by-role/2/'
prosumer_assignment_url = f'{base_url}/prosumers/assign/'
unassigned_prosumers_url = f'{base_url}/prosumers/unassigned/'
prosumer_energy_plan_url = f'{base_url}/prosumers/save_energy_plan/'

login_data = {
	'user_id': 'workshop@grit.systems',
	'password': 'haha'
}

new_energy_plan_data = {
	"name": "",
	"rate": "40.00",
	"power_limit": "1000",
	"energy_limit": "1000",
	"facility_id": ""
}

new_facility_data = {
	"name": "",
	"address": "Lagos",
	"type_of": "Market"
}

probe_data = {
	"consumers": [
		{
			"name": "SHOP1"
		},
		{
			"name": "SHOP2"
		},
		{
			"name": "SHOP3"
		},
		{
			"name": "SHOP4"
		},
		{
			"name": "SHOP5"
		},
		{
			"name": "SHOP6"
		},
		{
			"name": "SHOP7"
		},
		{
			"name": "SHOP8"
		}
	],
	"probe_id": "B8:27:EB:DB:CC:B0",
	"facility_id": ""
}

prosumer_assignment_data = {
	"group_id": "",
	"prosumer_id": "",
	"facility_id": ""
}

prosumer_energy_plan_data = {
	"prosumer_id": "",
	"plan_id": ""
}

def get_api_key(login_url, login_data):
	resp = requests.post(login_url, data=login_data)
	resp = resp.json()
	api_key = resp['token']
	return api_key

def get_facilities(headers, facility_url):
	resp = requests.get(facility_url, headers=headers)
	facilities = resp.json()
	return facilities

def create_new_facility(new_facility_url, new_facility_data, headers):
	resp = requests.post(new_facility_url, data=new_facility_data, headers=headers)
	facility = resp.json()
	return facility

def get_facility_id(headers, facility_name, facilities):
	if not facilities['data'].get(facility_name.upper()):
		new_facility_data['name'] = facility_name
		facilities = create_new_facility(new_facility_url, new_facility_data, headers)
	facility_id = facilities['data'].get(facility_name.upper())['facility_id']
	return facility_id

def create_new_energy_plan(new_energy_plan_url, new_energy_plan_data, headers):
	resp = requests.post(new_energy_plan_url, data=new_energy_plan_data, headers=headers)
	facility = resp.json()
	return facility

def get_energy_plan_id(headers, energy_plan_name, facility_id, energy_plans):
	if not energy_plans['data'].get(energy_plan_name.upper()):
		new_energy_plan_data['name'] = energy_plan_name
		new_energy_plan_data['facility_id'] = facility_id
		energy_plans = create_new_energy_plan(new_energy_plan_url, new_energy_plan_data, headers)
	energy_plan_id = energy_plans['data'].get(energy_plan_name.upper())['plan_id']
	return energy_plan_id

def get_energy_plans(facility_id, headers):
	url = energy_plan_url + facility_id + '/'
	resp = requests.get(url, headers=headers)
	energy_plans = resp.json()
	return energy_plans

def create_prosumers(headers, new_prosumer_url, facility_id):
	"""
		Method to create prosumers from a probe
	"""
	print("\t Creating Prosumers")
	probe_data['facility_id'] = facility_id
	resp = requests.post(new_prosumer_url, json=probe_data, headers=headers)
	probe = resp.json()
	print("\t Prosumer creation successful ", probe['success'])
	if not probe['success']:
		print("\t Reason: \t", probe['message'], resp.status_code)
	return probe

def get_unassigned_prosumers(headers, unassigned_prosumers_url, facility_id):
	url = unassigned_prosumers_url + facility_id + '/'
	resp = requests.get(url, headers=headers)
	unassigned_prosumers = resp.json()
	return unassigned_prosumers

def unassigned_prosumer_ids(unassigned_prosumers):
	unassigned_ids = []
	for prosumer in unassigned_prosumers['data']:
		unassigned_ids.append(prosumer['prosumer_id'])
	return unassigned_ids

def get_user_admins(headers, all_user_admins_url):
	resp = requests.get(all_user_admins_url, headers=headers)
	users = resp.json()
	return users

def get_user_group_ids(users):
	group_ids = []
	for user in users['data']:
		# print(user)
		# break
		group_ids.append(user['group_id'])
	return group_ids

def assign_user_to_prosumer(user_group_ids, unassigned_prosumer_ids, facility_id, headers):
	match = zip(user_group_ids, unassigned_prosumer_ids)
	for prosumer in match:
		prosumer_assignment_data['group_id'] = prosumer[0]
		prosumer_assignment_data['prosumer_id'] = prosumer[1]
		prosumer_assignment_data['facility_id'] = facility_id
		resp = requests.post(prosumer_assignment_url, data=prosumer_assignment_data, headers=headers)
		response = resp.json()
		success = response['success']
		print('\tProsumer Assignment Successful', success)
		if not success:
			print("\t Reason", response['errors'], resp.status_code)
		sleep(0.1)

def assign_energy_plan_to_prosumer(energy_plan_id, unassigned_prosumer_ids, headers):
	if unassigned_prosumer_ids is None:
		print("All Prosumers are assigned")
	for prosumer_id in unassigned_prosumer_ids:
		prosumer_energy_plan_data['prosumer_id'] = prosumer_id
		prosumer_energy_plan_data['plan_id'] = energy_plan_id
		resp = requests.post(prosumer_energy_plan_url, data=prosumer_energy_plan_data, headers=headers)
		response = resp.json()
		success = response['success']
		print('\tEnergy Assignment Successful ', success)

def main():
	facility_name = 'GRITDEVTest'
	energy_plan_name = 'GRITTest'
	api_key = get_api_key(login_url, login_data)
	headers = {'X-API-KEY': api_key}
	facilities = get_facilities(headers, facility_url)
	facility_id = get_facility_id(headers, facility_name, facilities)
	energy_plans = get_energy_plans(facility_id, headers)
	energy_plan_id = get_energy_plan_id(headers, energy_plan_name, facility_id, energy_plans)
	probe = create_prosumers(headers, new_prosumer_url, facility_id)
	users = get_user_admins(headers, all_user_admins_url)
	unassigned_prosumers = get_unassigned_prosumers(headers, unassigned_prosumers_url, facility_id)
	user_group_ids = get_user_group_ids(users)
	unassigned_ids = unassigned_prosumer_ids(unassigned_prosumers)
	assign_energy_plan_to_prosumer(energy_plan_id, unassigned_ids, headers)
	assign_user_to_prosumer(user_group_ids, unassigned_ids, facility_id, headers)
	pprint('facility_id = {}'.format(facility_id))

if __name__ == '__main__':
	main()