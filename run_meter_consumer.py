from simulator.producer.message_producer import begin_metering

print('Beginning electricity consumption.')
begin_metering(is_a_consumer=True)
