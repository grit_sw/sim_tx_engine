import os

from sqlalchemy import (Boolean, Column, Integer, String,
						create_engine, func)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
from sqlalchemy_utils import ArrowType

db_url = os.getenv('DATABASE_URL')
DeclarativeBase = declarative_base()
engine = create_engine(db_url)
session = Session(engine)

class Base(DeclarativeBase):
	__abstract__ = True
	column_id = Column(Integer, autoincrement=True, primary_key=True)
	date_created = Column(ArrowType, default=func.current_timestamp())
	date_modified = Column(ArrowType, default=func.current_timestamp(),
								onupdate=func.current_timestamp())


class Consumers(Base):
	__tablename__ = 'consumers'
	column_id = Column(Integer, autoincrement=True, primary_key=True)
	prosumer_id = Column(String(50))
	transaction_id = Column(String(50))
	start_time = Column(ArrowType)
	stop_time = Column(ArrowType)
	last_message_sent = Column(Boolean)

	@staticmethod
	def save_order(prosumer_id, transaction_id, start_time, stop_time):
		new_consumer = Consumers(
			prosumer_id=prosumer_id,
			transaction_id=transaction_id,
			start_time=start_time,
			stop_time=stop_time,
		)
		session.add(new_consumer)
		session.commit()


class Producers(Base):
	__tablename__ = 'producers'
	column_id = Column(Integer, autoincrement=True, primary_key=True)
	prosumer_id = Column(String(50))
	transaction_id = Column(String(50))
	start_time = Column(ArrowType)
	stop_time = Column(ArrowType)
	last_message_sent = Column(Boolean)

	@staticmethod
	def save_order(prosumer_id, transaction_id, start_time, stop_time):
		new_producer = Producers(
			prosumer_id=prosumer_id,
			transaction_id=transaction_id,
			start_time=start_time,
			stop_time=stop_time,
		)
		session.add(new_producer)
		session.commit()


# DeclarativeBase.metadata.drop_all(engine)
DeclarativeBase.metadata.create_all(engine)
