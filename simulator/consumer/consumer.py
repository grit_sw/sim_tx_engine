import os

import arrow
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError

from simulator.consumer.models import Consumers, Producers
from logger import logger


def create_consumer():
	c = AvroConsumer({
		'bootstrap.servers': os.getenv('KAFKA_URL'),
		# 'bootstrap.servers': 'dashboard.grit.systems',
		'group.id': os.getenv('METERING_GROUP'),
		'schema.registry.url': os.getenv('SCHEMA_REGISTRY_URL')})
		# 'schema.registry.url': 'http://dashboard.grit.systems:8081'})
	c.subscribe(['new-consumer-message', 'new-producer-message', 'probe-stream', 'gritServer'])
	return c

def start_consumer(c):
	while True:
		try:
			msg = c.poll(1)

		except SerializerError as e:
			logger.info("Message deserialization failed for {}: {}".format(msg, e))
			break

		if msg is None:
			continue

		if msg.value() == b'Broker: No more messages':
			logger.info(msg.value())
			continue

		if msg.topic() == 'new-consumer-message':
			message = msg.value()
			logger.info(f'New consumer message {message}')
			if not isinstance(message, dict):
				continue
			if 'start_time' in message:
				message['start_time'] = arrow.get(message['start_time']).datetime
				message['stop_time'] = arrow.get(message['stop_time']).datetime
				Consumers.save_order(**message)
			else:
				continue

		if msg.topic() in {'probe-stream', 'gritServer'}:
			message = msg.value()
			logger.info(f'New probe message {message}')

		if msg.topic() == 'new-producer-message':
			message = msg.value()
			logger.info(f'New producer message {message}')
			if not isinstance(message, dict):
				continue
			if 'start_time' in message:
				message['start_time'] = arrow.get(message['start_time']).datetime
				message['stop_time'] = arrow.get(message['stop_time']).datetime
				Producers.save_order(**message)
			else:
				continue

		if msg.error():
			logger.info("AvroConsumer error: {}".format(msg.error()))
			continue

# c.close()
