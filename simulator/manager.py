from calendar import monthrange

from arrow import now

# from simulator.messages.workshop_msgs import device_message
from simulator.messages.new_msg_format import device_message


class DeviceSimulator():
	"""docstring for Simulator"""

	def __init__(self, producer=None):
		super(DeviceSimulator, self).__init__()
		self.producer = producer
		self.device_message_func = device_message

	def device_message(self):
		return self.device_message_func(str(now()))
		# return self.device_message_func()

	def sim_device_messages(self):
		if self.producer is None:
			raise ValueError('Producer not found')
		message_func = self.device_message_func
		self.producer.produce_forever(message_func)

	def sim_monthly_messages(self):
		if self.producer is None:
			raise ValueError('Producer not found')
		message_func = self.device_message_func
		today = now().date()
		number_of_days_in_month = monthrange(today.year, today.month)[1]
		self.producer.produce_for_days(message_func, number_of_days_in_month)
