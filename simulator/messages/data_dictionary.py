"""This module contains a mapping of abbreviations used in the meter energy data format to their full names."""


class DuplicateKeysError(Exception):
    """Raise for duplicate data dictionary keys."""


data_dict = {
	"mas": "master",
	"data": "data",
	"cIDs": "configuration_IDs",
	"id": "id",
	"t": "time",
	"aS": "activeSource",
	"sN": "sourceName",
	"sT": "sourceType",
	"cFK": "configID_FK",
	"sFK": "sourceID_FK",
	"pSL": "powerSinceLast",
	"cSL": "costSinceLast",
	"eSL": "energySinceLast",
	"eTS": "energyTodaySource",
	"eT": "energyToday",
	"tTS": "timeTodaySource",
	"tT": "timeToday",
	"dod": "dod",
	"bCL": "batteryCapLast",
	"I": "current",
	"V": "voltage",
	"val": "value",
	"Type": "Type",
	"pF": "powerfactor",
	"cTS": "costTodaySource",
	"cT": "costToday",
	"fSL": "fuelSinceLast",
	"fHR": "fuelHourRate",
}

if len(data_dict.keys()) != len(set(data_dict.keys())):
	raise DuplicateKeysError("Duplicate keys found in the data dictionary")
print('Healthy Data Dictionary')