from random import uniform

import arrow


def device_message(current_time):
	a = {
			'data': [
				{
					'activeSource': [{'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'powerfactor': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'value': [0.800000011920929, 0, 0], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'value': [0, 0, 0], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'value': [0, 0.800000011920929, 0.800000011920929], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'costSinceLast': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'costSinceLast': uniform(0.0, 20.0), 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'costSinceLast': uniform(0.0, 20.0), 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'costSinceLast': uniform(0.0, 200.0), 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'powerSinceLast': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'powerSinceLast': uniform(0.0, 2000000.0)}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'powerSinceLast': uniform(0.0, 2000000.0)}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'powerSinceLast': uniform(0.0, 2000000.0)}],
					'energyTodaySource': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'energyToday': uniform(0.0, 2000000.0), 'sourceType': 'inverter', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'energyToday': uniform(0.0, 2000000.0), 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'energyToday': uniform(0.0, 2000000.0), 'sourceType': 'grid', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'timeTodaySource': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'timeToday': 0.008708867244422436}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'timeToday': 0}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'timeToday': 12.985160827636719}],
					'dod': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'dod': 0.03207842633128166, 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'energySinceLast': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'energySinceLast': 1.8094745874404907}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'energySinceLast': 0}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'energySinceLast': 18.608800888061523}],
					'current': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'value': [uniform(0.0, 20.0), 0, 0], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'value': [0, 0, uniform(0.0, 20.0)], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'value': [uniform(0.0, 50.0), uniform(0.0, 50.0), uniform(0.0, 50.0)], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'fuelSinceLast': [{'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'fuelSinceLast': uniform(0.0, 10.0), 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'fuelHourRate': [{'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'fuelHourRate': uniform(0.0, 10.0), 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'voltage': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'value': [uniform(230.0, 250.0), 0, 0], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'sourceType': 'generator', 'value': [uniform(230.0, 250.0), uniform(230.0, 250.0), uniform(230.0, 250.0)], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'sourceType': 'grid', 'value': [uniform(230.0, 250.0), uniform(230.0, 250.0), uniform(230.0, 250.0)], 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
					'time': current_time,
					'batteryCapLast': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'sourceType': 'inverter', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', 'batteryCapLast': uniform(50.0, 200.0)}],
					'Type': '1',
					'id': 'B8:27:EB:26:8F:40',
					'costTodaySource': [{'sourceId': 'TestSourceId', 'sourceName': 'MUST4024C', 'costToday': uniform(0.0, 20000.0), 'sourceType': 'inverter', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'PERKINSP40P3', 'costToday': uniform(0.0, 20000.0), 'sourceType': 'generator', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}, {'sourceId': 'TestSourceId', 'sourceName': 'EKODISCO', 'costToday': uniform(0.0, 20000.0), 'sourceType': 'grid', 'configID_FK': 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}]
				}
			],
			'master': {'configuration_IDs': ['G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'], 'id': 'B8:27:EB:26:8F:40', 'time': current_time}
			}
	return a
