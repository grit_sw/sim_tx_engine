from random import choice, uniform

def device_message(current_time):
	n = {
		'mas': {
			'cIDs': [u'UD5T4WU2QV52FLSO'], 
			'id': u'B8:27:EB:DB:CC:B0',
			't': u'2018-06-08T10:40:46.017+0100'
			}, 
			'data': [
				{
				'aS': [
					{
						'sN': u'EKODISCO', 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'pSL': [
					{
						'sN': u'EKODISCO', 
						'pSL': round(uniform(1000, 20000), 4), 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'pSL': round(uniform(1000, 20000), 4), 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'pSL': round(uniform(1000, 20000), 4), 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'pSL': round(uniform(1000, 20000), 4), 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'pSL': round(uniform(500, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'pSL': round(uniform(500, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'pSL': round(uniform(500, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'pSL': round(uniform(500, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'cSL': [
					{
						'sN': u'EKODISCO', 
						'cSL': round(uniform(20, 50), 4), 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'cSL': round(uniform(20, 50), 4), 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'cSL': round(uniform(20, 50), 4), 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'cSL': round(uniform(20, 50), 4), 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'cSL': round(uniform(0, 10), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'cSL': round(uniform(0, 10), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'cSL': round(uniform(0, 20), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'cSL': round(uniform(0, 20), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'eSL': [
					{
						'sN': u'EKODISCO', 
						'eSL': round(uniform(10, 500), 4), 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'eSL': round(uniform(10, 500), 4), 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'eSL': round(uniform(10, 500), 4), 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'eSL': round(uniform(10, 500), 4), 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'eSL': round(uniform(1, 50), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'eSL': round(uniform(1, 50), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'eSL': round(uniform(1, 50), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'eSL': round(uniform(1, 50), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'eTS': [
					{
						'sN': u'EKODISCO', 
						'eT': round(uniform(1000, 50000), 4), 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'eT': round(uniform(1000, 50000), 4), 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'eT': round(uniform(1000, 50000), 4), 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'eT': round(uniform(1000, 50000), 4), 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'eT': round(uniform(100, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'eT': round(uniform(100, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'eT': round(uniform(100, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'eT': round(uniform(100, 5000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'tTS': [
					{
						'sN': u'EKODISCO', 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO', 
						'tT': round(uniform(1, 24), 4),
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'dod': [], 
				'id': u'B8:27:EB:DB:CC:B0', 
				'I': [
					{
						'sN': u'EKODISCO', 
						'sT': u'grid', 
						'val': [round(uniform(10, 50), 4), round(uniform(10, 50), 4), round(uniform(10, 50), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'sT': u'generator', 
						'val': [round(uniform(10, 50), 4), round(uniform(10, 50), 4), round(uniform(10, 50), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'sT': u'inverter', 
						'val': [round(uniform(10, 50), 4), round(uniform(10, 50), 4), round(uniform(10, 50), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'sT': u'solar', 
						'val': [round(uniform(10, 50), 4), round(uniform(10, 50), 4), round(uniform(10, 50), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'sT': u'consumer', 
						'val': [round(uniform(10, 50), 4), 0, 0, ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'sT': u'consumer', 
						'val': [0, round(uniform(10, 50), 4), 0, ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'sT': u'consumer', 
						'val': [0, 0, round(uniform(10, 50), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'sT': u'consumer', 
						'val': [round(uniform(10, 50), 4), 0, 0, ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'fSL': [], 
				'fHR': [], 
				'V': [
					{
						'sN': u'EKODISCO', 
						'sT': u'grid', 
						'val': [round(uniform(238, 242), 4), round(uniform(238, 242), 4), round(uniform(238, 242), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'sT': u'generator', 
						'val': [round(uniform(238, 242), 4), round(uniform(238, 242), 4), round(uniform(238, 242), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'sT': u'inverter', 
						'val': [round(uniform(238, 242), 4), round(uniform(238, 242), 4), round(uniform(238, 242), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'sT': u'solar', 
						'val': [round(uniform(238, 242), 4), round(uniform(238, 242), 4), round(uniform(238, 242), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'sT': u'consumer', 
						'val': [round(uniform(238, 242), 4), 0, 0, ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'sT': u'consumer', 
						'val': [0, round(uniform(238, 242), 4), 0, ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'sT': u'consumer', 
						'val': [0, 0, round(uniform(238, 242), 4), ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'sT': u'consumer', 
						'val': [round(uniform(238, 242), 4), 0, 0, ], 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				't': u'2018-06-08T10:40:46.017+0100', 
				'bCL': [], 
				'Type': u'1', 
				'pF': [
					{
						'sN': u'EKODISCO', 
						'sT': u'grid', 
						'val': [0.9, 0.9, 0.9, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'sT': u'generator', 
						'val': [0.9, 0.9, 0.9, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'sT': u'inverter', 
						'val': [0.9, 0.9, 0.9, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'sT': u'solar', 
						'val': [0.9, 0.9, 0.9, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'sT': u'consumer', 
						'val': [0.9, 0, 0, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'sT': u'consumer', 
						'val': [0, 0.9, 0, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'sT': u'consumer', 
						'val': [0, 0, 0.9, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'sT': u'consumer', 
						'val': [0.9, 0, 0, ],
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				], 
				'cTS': [
					{
						'sN': u'EKODISCO', 
						'cT': round(uniform(200000, 10000000), 4), 
						'sT': u'grid', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'AAAAAAAAAAAAAAAA',
					}, 
					{
						'sN': u'PERKINS', 
						'cT': round(uniform(200000, 10000000), 4), 
						'sT': u'generator', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'BBBBBBBBBBBBBBBB',
					}, 
					{
						'sN': u'LUMINOUS', 
						'cT': round(uniform(200000, 10000000), 4), 
						'sT': u'inverter', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'CCCCCCCCCCCCCCCC',
					}, 
					{
						'sN': u'SIMBA', 
						'cT': round(uniform(200000, 10000000), 4), 
						'sT': u'solar', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'DDDDDDDDDDDDDDDD',
					}, 
					{
						'sN': u'SHOP1', 
						'cT': round(uniform(500, 1000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'EEEEEEEEEEEEEEEE',
					}, 
					{
						'sN': u'SHOP2', 
						'cT': round(uniform(100, 500), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'FFFFFFFFFFFFFFFF',
					}, 
					{
						'sN': u'SHOP3', 
						'cT': round(uniform(10000, 50000), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'GGGGGGGGGGGGGGGG',
					}, 
					{
						'sN': u'SHOP4', 
						'cT': round(uniform(5, 10), 4), 
						'sT': u'consumer', 
						'cFK': u'UD5T4WU2QV52FLSO',
						'sFK': u'HHHHHHHHHHHHHHHH',
					}
				]
			}
		]
	}
	return n