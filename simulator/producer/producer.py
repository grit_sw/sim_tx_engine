from time import sleep, time
import arrow

from confluent_kafka.avro import AvroProducer
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry

from logger import logger


class Producer(object):
	"""docstring for Producer"""

	def __init__(self, bootstrap_servers=None, schema_registry_url=None, group_id=None, topic=None, schema_subject=None):
		super(Producer, self).__init__()
		self.bootstrap_servers = bootstrap_servers
		self.schema_registry_url = schema_registry_url
		self.schema_registry = SchemaRegistry(self.schema_registry_url)
		self.group_id = group_id
		self.topic = topic
		self.schema_subject = schema_subject
		self.avro_producer = AvroProducer({
			'bootstrap.servers': self.bootstrap_servers,
			'schema.registry.url': self.schema_registry_url,
		},
			default_value_schema=self.value_schema
		)

	def __repr__(self):
		return f'<Producer topic={self.topic}>'

	@property
	def value_schema(self):
		if self.schema_subject is None or self.schema_registry is None:
			raise ValueError('Schema subject AND schema_registry required')
		_, schema, _ = self.schema_registry.get_latest_schema(self.schema_subject)
		return schema

	def send_message(self, message):
		if not message:
			raise ValueError("Message is required")

		self.avro_producer.produce(topic=self.topic, value=message)
		self.avro_producer.flush()
		# sleep(0.000001)


class SimProducer(Producer):
	"""Inherits Kafka producer class"""

	def __init__(self, bootstrap_servers, schema_registry_url, group_id, topic, schema_subject):
		Producer.__init__(self, bootstrap_servers, schema_registry_url, group_id, topic, schema_subject)
		self.bootstrap_servers = bootstrap_servers
		self.schema_registry_url = schema_registry_url
		self.group_id = group_id
		self.topic = topic
		self.schema_subject = schema_subject
		# self.value_schema = self.value_schema
	
	def create_message(self, message_func, count, current_time=str(arrow.now())):
		message = message_func(current_time=current_time)
		logger.info('\nMessage count = {}'.format(count))
		logger.info('Topic: {}'.format(self.topic))
		logger.info('Subject: {}'.format(self.schema_subject))
		# logger.info('Duration (Seconds): {} seconds'.format(time() - t1))
		# logger.info('Velocity {} messages per second'.format(count / (time() - t1)))
		# logger.info('Velocity {} messages per minute'.format(count / ((time() - t1) / 60)))

		# logger.info(message)
		self.send_message(message)
		# sleep(randrange(2, 35))
		# break
		sleep(.1)
		# sleep(0.0000000000000000001)
		# sleep(0.000001)

	def produce_one_message(self, message):
		"""Dev Function"""
		self.send_message(message)

	def produce_for_days(self, message_func, days):
		count = 1
		messages_per_day = 20
		t1 = time()
		for i in range(days):
			current_time = str(arrow.now().replace(day=i+1))
			for j in range(messages_per_day):
				self.create_message(message_func, count, current_time=current_time)
				count += 1
				if count % 50 == 0:
					sleep(5)
		logger.info("\n\tTook {} seconds to process {}".format(time() - t1, count))

	def produce_forever(self, message_func):
		"""Dev Function"""
		# while True:
		t1 = time()
		number_of_messages = 13614
		count = 1
		# for i in range(number_of_messages):
		while True:
			self.create_message(message_func, count)
			count += 1
			logger.info('Duration (Minutes): {} minutes'.format((time() - t1) / 60))
		time_taken = time() - t1
		logger.info("\n\tTook {} seconds to process {}".format(time() - t1, number_of_messages))
		logger.info("\tProducer speed = {} messages / second".format(number_of_messages / time_taken))
