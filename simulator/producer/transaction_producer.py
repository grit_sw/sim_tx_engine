import os
from uuid import uuid4

import arrow
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry

from logger import logger
from simulator.producer.producer import Producer
from simulator.producer.register_schema import RegisterSchema

broker_url = os.getenv('KAFKA_URL')
schema_subject = os.getenv('ORDER_SCHEMA_SUBJECT')
schema_registry = os.getenv('SCHEMA_REGISTRY_URL')
schema_file = os.getenv('ORDER_SCHEMA_FILE')

consumer_topic = os.getenv('CONSUMPTION_REQUEST_TOPIC')
producer_topic = os.getenv('PRODUCTION_REQUEST_TOPIC')

_, b, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
if b is None:
	register_schema = RegisterSchema(schema_registry, subject=schema_subject, file=schema_file)
	register_schema.post_schema()
	logger.info('New schema registration at {} with subject {}\n'.format(schema_registry, schema_subject))
else:
	logger.info('Schema already registered at {} with subject {}\n'.format(schema_registry, schema_subject))

avro_producer = Producer(broker_url, schema_registry,
					   'ariaria_transact_dev', consumer_topic, schema_subject=schema_subject)


def request_production():
	message = {
		'start_time': str(arrow.now()),
		'stop_time': str(arrow.now().shift(minutes=1)),
		'prosumer_id': 'TE:ST:PR:OB:01',
		'transaction_id': str(uuid4())
	}
	avro_producer.topic = producer_topic
	avro_producer.send_message(message)
	logger.info('Sent production request.')
	logger.info(f'{avro_producer}')

def request_consumption():
	message = {
		'start_time': str(arrow.now()),
		'stop_time': str(arrow.now().shift(minutes=1)),
		'prosumer_id': 'TE:ST:PR:OB:02',
		'transaction_id': str(uuid4())
	}
	avro_producer.topic = consumer_topic
	avro_producer.send_message(message)
	logger.info('Sent consumption request.')
	logger.info(f'{avro_producer}')
