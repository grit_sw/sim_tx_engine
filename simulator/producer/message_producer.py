import os
from time import sleep

import arrow
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry

from logger import logger
from simulator.consumer.models import Consumers, Producers, session
from simulator.messages.workshop_msgs import device_message
from simulator.producer.producer import Producer
from simulator.producer.register_schema import RegisterSchema
from simulator.producer.transaction_producer import (request_consumption,
                                                     request_production)

topic = os.getenv('METERING_TOPIC')
last_consumer_message_topic = os.getenv('LAST_CONSUMER_MESSAGE')
last_producer_message_topic = os.getenv('LAST_PRODUCER_MESSAGE')
broker_url = os.getenv('KAFKA_URL')
schema_subject = os.getenv('METERING_SCHEMA_SUBJECT')
schema_registry = os.getenv('SCHEMA_REGISTRY_URL')
schema_file = os.getenv('METERING_SCHEMA_FILE')
group_id = os.getenv('ORDER_GROUP')

_, b, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
if b is None:
	register_schema = RegisterSchema(schema_registry, subject=schema_subject, file=schema_file)
	register_schema.post_schema()
	logger.info('New schema registration at {} with subject {}\n'.format(schema_registry, schema_subject))
else:
	logger.info('Schema already registered at {} with subject {}\n'.format(schema_registry, schema_subject))

avro_producer = Producer(broker_url, schema_registry,
					   group_id, topic, schema_subject=schema_subject)


def begin_metering(is_a_consumer=True):
	Table = Consumers
	if not is_a_consumer:
		Table = Producers
	sleep_duration = 5
	count = 1
	no_msgs_count = 0
	while True:
		# logger.info(f'Count = {count}')
		prosumers = session.query(Table).all()
		if prosumers:
			logger.info(f'Preparing to send messages for {len(prosumers)} {Table.__tablename__}')
			for prosumer in prosumers:
				prosumer_id = prosumer.prosumer_id
				transaction_id = prosumer.transaction_id
				last_message_sent = prosumer.last_message_sent
				start_time = prosumer.start_time.to('Africa/Lagos')
				stop_time = prosumer.stop_time.to('Africa/Lagos')
				current_time = arrow.now('Africa/Lagos')

				if current_time > start_time and current_time < stop_time:
					is_last_message = check_last_message(prosumer, sleep_duration)
					message = device_message(str(current_time), prosumer_id, transaction_id, is_last_message)
					if is_last_message:
						send_last_message(message, is_a_consumer=is_a_consumer)
						save_last_message(prosumer)
						delete_prosumer(prosumer, is_a_consumer=is_a_consumer)
						logger.info(f'Last metering message sent for meter: {prosumer_id} at {current_time}\n')
					else:
						send_metering_message(message)
					logger.info(f'Metering message sent for meter: {prosumer_id} at {current_time}\n')

				elif current_time > start_time and not last_message_sent:
					is_last_message = check_last_message(prosumer, sleep_duration)
					message = device_message(str(current_time), prosumer_id, transaction_id, is_last_message)
					send_last_message(message, is_a_consumer=is_a_consumer)
					save_last_message(prosumer)
					delete_prosumer(prosumer, is_a_consumer=is_a_consumer)
					logger.info(f'Last metering message sent for meter: {prosumer_id} at {current_time}\n')

				else:
					session.query(Table).filter_by(prosumer_id=prosumer_id).filter_by(start_time=start_time).delete()
					session.query(Table).filter_by(last_message_sent=True).delete()
					session.commit()

		else:
			prosumer_adj = 'consumption'
			if not is_a_consumer:
				prosumer_adj = 'production'
			logger.info(f'No electricity {prosumer_adj} requests at this time.')
			logger.info('Disturb the marketing department.\n')
			no_msgs_count += 1
			if no_msgs_count % 5 == 0:
				if is_a_consumer:
					request_consumption()
				else:
					request_production()
				no_msgs_count = 0
		sleep(sleep_duration)
		count += 1

def delete_prosumer(prosumer, is_a_consumer=False):
	Table = Producers
	if is_a_consumer:
		Table = Consumers
	session.query(Table).filter_by(prosumer_id=prosumer.prosumer_id).filter_by(start_time=prosumer.stop_time).delete()
	session.commit()

def send_metering_message(message):
	avro_producer.send_message(message)

def send_last_message(message, is_a_consumer=False):
	if is_a_consumer:
		avro_producer.topic = last_consumer_message_topic
	else:
		avro_producer.topic = last_producer_message_topic
	avro_producer.send_message(message)

def save_last_message(prosumer):
	prosumer.last_message_sent = True
	session.add(prosumer)
	session.commit()

def check_last_message(prosumer, next_run):
	stop_time = arrow.get(prosumer.stop_time)
	next_run_time = arrow.now().shift(seconds=next_run)
	is_last_message = False
	if next_run_time > stop_time:
		is_last_message = True
	return is_last_message
