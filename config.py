import os


class Config(object):
	DEBUG = False
	SECRET_KEY = os.urandom(24)
	AUTH_PORT = os.environ['AUTH_PORT']
	ACCOUNT_PORT = os.environ['ACCOUNT_PORT']
	BUYORDER_PORT = os.environ['BUYORDER_PORT']
	PAYSTACK_PORT = os.environ['PAYSTACK_PORT']
	PIN_PORT = os.environ['PIN_PORT']
	USERS_PORT = os.environ['USERS_PORT']
	SELLORDER_PORT = os.environ['SELLORDER_PORT']
	ENERGYPLAN_PORT = os.environ['ENERGYPLAN_PORT']
	PROSUMER_PORT = os.environ['PROSUMER_PORT']
	CONFIGURATION_PORT = os.environ['CONFIGURATION_PORT']
	ORDERUTILS_PORT = os.environ['ORDERUTILS_PORT']
	FEEDBACK_PORT = os.environ['FEEDBACK_PORT']
	SUBNET_PORT = os.environ['SUBNET_PORT']

	@staticmethod
	def init_app(app):
		pass


class Development(Config):
	HOST = 'http://localhost:'


class Staging(Config):
	HOST = 'http://docs.gtg.grit.systems:'


config = {
	'dev': Development,
	'development': Development,
	'Staging': Staging,
}
