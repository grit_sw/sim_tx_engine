#!/usr/bin/env python3
import os

from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry

from simulator.manager import DeviceSimulator
from simulator.producer.producer import SimProducer
from simulator.producer.register_schema import RegisterSchema

topic = 'probe-stream'
# topic = 'gritServer'
broker_url = os.getenv('KAFKA_URL')
schema_subject = os.getenv('METERING_SCHEMA_SUBJECT')
schema_registry = os.getenv('SCHEMA_REGISTRY_URL')
schema_file = os.getenv('METERING_SCHEMA_FILE')
_, b, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
if b is None:
	register_schema = RegisterSchema(schema_registry, subject=schema_subject, file=schema_file)
	register_schema.post_schema()
	print('New schema registration at {} with subject {}\n'.format(schema_registry, schema_subject))
else:
	print('Schema already registered at {} with subject {}\n'.format(schema_registry, schema_subject))

producer = SimProducer(broker_url, schema_registry,
					   'ariaria_transact_dev', topic, schema_subject=schema_subject)
simulator = DeviceSimulator(producer=producer)
simulator.sim_monthly_messages()
