"""
    Script to Create buy orders for a random consumer in a subnet.
"""

import requests
import arrow
import random

base_url = 'http://dos.grit.systems:5500'
non_base_url = 'http://localhost:5700'
all_subnets_url = f'{base_url}/subnet/all/'
subnet_consumers_url = f'{base_url}/prosumers/consumers-in-subnet/'
subnet_producers_url = f'{base_url}/prosumers/producers-in-subnet/'
# subnet_producers_url = f'http://localhost:5700/prosumers/producers-in-subnet/'
new_buy_order_url = f'{base_url}/buy/self-new-order/'
new_sell_order_url = f'{base_url}/sell/self-new-order/'
billing_method_url = f'{base_url}/billing-method/view-all/'
billing_unit_url = f'{base_url}/billing-unit/all/'
time_span_measure_url = f'{base_url}/time-span-measure/view-all/'
renew_frequency_url = f'{base_url}/renew-frequency/view-all/'
login_url = f'{base_url}/auth/login/'

cookies = {'role_id': '6', 'group_id': 'string'}

login_data = {
	'user_id': 'workshop@grit.systems',
	'password': 'haha'
}

buy_order_data = {
    "billing_method_id": "string",
    "billing_unit_id": "string",
    "rate": 0,
    "cost_limit": 0,
    "time_span": 0,
    "time_span_measure_id": "string",
    "auto_renew": True,
    "renew_frequency": "string",
    "start_date_time": "string",
    "end_date_time": "string",
    "user_group_id": "string",
    "facility_id": "string",
    "subnet_id": "string",
    "prosumer_id": "string"
}

def get_api_key():
	resp = requests.post(login_url, data=login_data)
	resp = resp.json()
	api_key = resp['token']
	return api_key, resp

def query_for_subnet(headers):
    return requests.get(all_subnets_url, cookies=cookies, headers=headers).json()['data']

def query_for_simulator_subnet(all_subnets, headers):
    if all_subnets:
        for subnet in iter(all_subnets):
            name = subnet['name']
            if name != "Simulator Subnet":
                continue
            subnet_id = subnet['subnet_id']
            return subnet_id
        raise Exception('Simulator subnet not found.')
    raise Exception('No Subnets found.')

def get_consumer_from_subnet(subnet_id, headers):
    consumers = requests.get(subnet_consumers_url + subnet_id + '/', cookies=cookies, headers=headers).json()['data']
    print(f"There are {len(consumers)} consumers in this subnet.")
    if consumers:
        return random.choice(consumers)
    raise Exception('No consumers in this subnet.')

def get_producer_from_subnet(subnet_id, headers):
    producers = requests.get(subnet_producers_url + subnet_id + '/', cookies=cookies, headers=headers).json()['data']
    print(f"There are {len(producers)} producers in this subnet.")
    if producers:
        return random.choice(producers)
    raise Exception('No producers in this subnet.')

def query_for_consumers_in_one_subnet(subnet_id, headers):
    consumers = requests.get(subnet_consumers_url + subnet_id + '/', cookies=cookies, headers=headers).json()['data']
    if consumers:
        return random.choice(consumers)

def get_billing_method(headers):
    return random.choice(requests.get(billing_method_url, cookies=cookies, headers=headers).json()['data'])['billing_method_id']

def get_billing_unit(headers):
    return random.choice(requests.get(billing_unit_url, cookies=cookies, headers=headers).json()['data'])['billing_unit_id']

def get_time_span_measure(headers):
    return random.choice(requests.get(time_span_measure_url, cookies=cookies, headers=headers).json()['data'])['time_span_measure_id']

def get_renew_frequency(headers):
    return random.choice(requests.get(renew_frequency_url, cookies=cookies, headers=headers).json()['data'])['renew_frequency_id']

def create_buy_order(consumer, billing_method_id, billing_unit_id, rate, start_time, stop_time, headers):
    buy_order_data['billing_method_id'] = billing_method_id
    buy_order_data['billing_unit_id'] = billing_unit_id
    buy_order_data['rate'] = rate
    buy_order_data['cost_limit'] = random.randrange(1000, 5000)
    buy_order_data['time_span'] = random.randint(1, 9)
    buy_order_data['time_span_measure_id'] = get_time_span_measure(headers)
    buy_order_data['auto_renew'] = bool(random.getrandbits(1))
    buy_order_data['renew_frequency'] = random.choice(['Daily', 'Weekly', 'Monthly'])
    buy_order_data['start_date_time'] = start_time
    buy_order_data['end_date_time'] = stop_time
    buy_order_data['user_group_id'] = consumer['user_group_id']
    buy_order_data['facility_id'] = consumer['facility_id']
    buy_order_data['subnet_id'] = consumer['subnets'][0]['subnet_id']
    buy_order_data['prosumer_id'] = consumer['prosumer_id']

    print(f'buy order start time {start_time} stop time {stop_time}')
    resp = requests.post(new_buy_order_url, json=buy_order_data, cookies=cookies, headers=headers)
    return resp.ok, resp.status_code

def create_sell_order(producer, billing_unit_id, rate, start_time, stop_time, headers):
    sell_order_data = {}
    sell_order_data['billing_unit_id'] = billing_unit_id
    sell_order_data['rate'] = rate
    sell_order_data['power_limit'] = random.randrange(5000, 50000)
    sell_order_data['energy_limit'] = random.randrange(5000, 50000)
    sell_order_data['auto_renew'] = bool(random.getrandbits(1))
    sell_order_data['renew_frequency'] = random.choice(['Daily', 'Weekly', 'Monthly'])
    sell_order_data['start_date_time'] = start_time
    sell_order_data['end_date_time'] = stop_time
    sell_order_data['user_group_id'] = producer['user_group_id']
    sell_order_data['facility_id'] = producer['facility_id']
    sell_order_data['subnet_id'] = producer['subnets'][0]['subnet_id']
    sell_order_data['prosumer_id'] = producer['prosumer_id']

    print(f'sell order start time {start_time} stop time {stop_time}')
    resp = requests.post(new_sell_order_url, json=sell_order_data, cookies=cookies, headers=headers)
    return resp.ok, resp.status_code

def create_order(consumer, producer, headers):
    billing_unit_id = get_billing_unit(headers)
    billing_method_id = get_billing_method(headers)
    rate = random.randrange(10, 55)
    start_shift = 7
    stop_shift = 17
    while True:
        start_time = str(arrow.now('Africa/Lagos').shift(minutes=start_shift))
        stop_time = str(arrow.now('Africa/Lagos').shift(minutes=stop_shift))
        status_ok, status_code = create_buy_order(consumer, billing_method_id, billing_unit_id, rate, start_time, stop_time, headers)
        print(f'Buy order status = {status_code}')
        if int(status_code) == 409:
            print('Searching for suitable order schedule.')
            difference = stop_shift - start_shift
            start_shift = stop_shift
            stop_shift = stop_shift + difference
            continue
        if not status_ok:
            return status_ok
        break
    status_ok, status_code = create_sell_order(producer, billing_unit_id, rate, start_time, stop_time, headers)
    print(f'Sell order status = {status_code}')
    return status_ok

def new_fake_order():
    print('Retrieving api keys')
    api_key, _ = get_api_key()
    headers = {'X-API-KEY': api_key}

    print('Querying for simulator subnet.\n')
    all_subnets = query_for_subnet(headers)
    subnet_id = query_for_simulator_subnet(all_subnets, headers)

    print('Querying for consumers in subnet\n')
    consumer = get_consumer_from_subnet(subnet_id, headers)

    print('Querying for producers in subnet\n')
    producer = get_producer_from_subnet(subnet_id, headers)
    # print(producer)
    
    print('Creating new buy and sell orders.\n')
    created = create_order(consumer, producer, headers)
    print(f'New orders created - {created}\n')

    response = {}

    response['success'] = True
    response['message'] = f"Orders successfully created - {created}."
    return response, 201 if created else 400

# new_fake_order()