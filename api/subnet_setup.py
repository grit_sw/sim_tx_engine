"""
	ONLY RUN THIS IN A DEVELOPMENT ENVIRONMENT, AGAINST A DEVELOPMENT DATABASE.
	Script to setup a subnet to be used by the development team.
"""
import string
from pprint import pprint
from random import choice, choices, randrange, sample
from time import sleep

import arrow
import requests
from forgery_py.forgery.address import street_address
from forgery_py.forgery.internet import email_address
from forgery_py.forgery.name import company_name, full_name, first_name

base_url = 'http://dos.grit.systems:5500'
non_base_url = 'http://localhost:5700'
cookies = {'role_id': '6'}
three_phase_generator_url = f'{base_url}/three-phase-generator/uninstalled/all/'
three_phase_consumer_url = f'{base_url}/three-phase-consumer/uninstalled/all/'
single_phase_consumer_url = f'{base_url}/single-phase-consumer/uninstalled/all/'
all_facilities_url = f'{base_url}/facility/all/'
circuit_type_url = f'{base_url}/subnet/circuit-type/all/'
subnet_type_url = f'{base_url}/subnet/subnet-type/all/'
current_type_url = f'{base_url}/subnet/current-type/all/'
new_subnet_url = f'{base_url}/subnet/new/'
new_fake_user_url = f'{base_url}/users/new-fake-user/'
bulk_credit_account_url = f'{base_url}/account/bulk-credit/'
prosumer_types_url = f'{base_url}/prosumer-type/all/'
bulk_new_prosumer_groups_url = f'{base_url}/prosumer-group/bulk-new-group/'
bulk_new_prosumers_url = f'{base_url}/prosumers/bulk-new/'
# bulk_new_prosumers_url = f'http://localhost:5700/prosumers/bulk-new/'
login_url = f'{base_url}/auth/login/'
# facility_url = f'{base_url}/facility/all/'
# new_facility_url = f'{base_url}/facility/new/'
# new_prosumer_url = f'{base_url}/prosumers/new/'
# all_user_admins_url = f'{base_url}/users/by-role/2/'
# prosumer_assignment_url = f'{base_url}/prosumers/assign/'
# unassigned_prosumers_url = f'{base_url}/prosumers/unassigned/'
# prosumer_energy_plan_url = f'{base_url}/prosumers/save_energy_plan/'

login_data = {
	'user_id': 'workshop@grit.systems',
	'password': 'haha'
}

new_subnet_data = {
  "name": "Simulator Subnet",
  "circuit_type_id": "string",
  "subnet_type_id": "string",
  "current_type_id": "string",
  "facilities": "string",
  "transformers": [
	{
	  "name": "SIMULATOR SUBNET TRANSFORMER",
	  "voltage_rating": 240,
	  "power_rating": 50400,
	  "frequency_rating": 50,
	  "rmus": [
		{
		  "name": "SIMULATOR SUBNET RMU 1",
		  "voltage_rating": 240,
		  "current_rating": 210
		}
	  ]
	}
  ]
}

def create_random_string(N):
	return ''.join(choices(string.ascii_uppercase + string.digits, k=N))

credit_account_data = {
	"paid_at": str(arrow.now()),
	"group_id": "",
	"reference": '',
	"amount_paid": "",
	"customer_id": "",
	"tx_performed_by": "",
	"is_working_balance": True,
	"paystack_auth": '',
	"last_four": 0,
	"brand": "MFA",
	"meter_id": '',
	"energy_units_purchased": "20000"
}

new_prosumer_data = {
	"prosumer_name": "",
	"user_group_id": "",
	"device_id": "",
	"prosumer_type_id": "",
	"prosumer_group_id": "",
	"facility_id": "",
	"subnet_id": "",
	"probe_names": [
		"string"
	]
}
def get_api_key():
	print('Retrieving Api Key.')
	resp = requests.post(login_url, data=login_data)
	resp = resp.json()
	api_key = resp['token']
	print('Api key received.')
	return api_key, resp

def get_circuit_type_id(headers):
	resp = requests.get(circuit_type_url, cookies=cookies, headers=headers)
	if resp.ok:
		return resp.json()['data'][0]['circuit_type_id']
	else:
		resp.raise_for_status()

def get_subnet_type_id(headers):
	resp = requests.get(subnet_type_url, cookies=cookies, headers=headers)
	if resp.ok:
		return resp.json()['data'][0]['subnet_type_id']
	else:
		resp.raise_for_status()

def get_current_type_id(headers):
	resp = requests.get(current_type_url, cookies=cookies, headers=headers)
	if resp.ok:
		return resp.json()['data'][0]['current_type_id']
	else:
		resp.raise_for_status()

def get_three_phase_generators(count, headers):
	print(f'Obtaining {count} three phase generators.')
	resp = requests.get(three_phase_generator_url, headers=headers, cookies=cookies)
	resp = resp.json()['data']
	print(f"Number of three phase generators retrieved {len(resp)}")
	return resp[:count]

def get_three_phase_consumers(count, headers):
	print(f'Obtaining {count} three phase consumers.')
	resp = requests.get(three_phase_consumer_url, headers=headers, cookies=cookies)
	resp = resp.json()['data']
	print(f"Number of three phase consumers retrieved {len(resp)}")
	return resp[:count]

def get_one_phase_consumers(count, headers):
	print(f'Obtaining {count} single phase consumers.')
	resp = requests.get(single_phase_consumer_url, headers=headers, cookies=cookies)
	resp = resp.json()['data']
	print(f"Number of single phase consumers retrieved {len(resp)}")
	return resp[:count]

def create_unique_items(count, func):
	seq = []
	while True:
		r = list(set([func() for _ in range(count)]))
		if len(r) == count:
			seq = r
			break
	return seq

def get_facilities(count, headers):
	print(f'Retrieving {count} facilities.')
	cookies = {'role_id': '5'}
	resp = requests.get(all_facilities_url, cookies=cookies, headers=headers)
	if resp.ok:
		resp = resp.json()['data']
		facility_ids = resp[:count]
		return facility_ids
	resp.raise_for_status()

def create_subnet(facilities, cookies, headers):
	print('Creating subnet')
	facility_ids = [facility['facility_id'] for facility in facilities]
	new_subnet_data['circuit_type_id'] = get_circuit_type_id(headers)
	new_subnet_data['subnet_type_id'] = get_subnet_type_id(headers)
	new_subnet_data['current_type_id'] = get_current_type_id(headers)
	new_subnet_data['facilities'] = facility_ids
	resp = requests.post(new_subnet_url, json=new_subnet_data, cookies=cookies, headers=headers)
	resp = resp.json()['data']
	print(f"Subnet {resp['name']} created.")
	return resp


def p_number():
	"""Generates random phonenumbers"""
	return '0{a}{b}{c}{d}'.format(a=str(choice([7, 8])), b=str(choice([0, 1])), c=str(choice([2, 9])), d=str(''.join(str(choice(list(range(0, 9)))) for _ in range(8))))

def create_user_per_facility(facilities, headers):
	facility_ids = [facility['facility_id'] for facility in facilities]
	print(f'Creating {len(facility_ids)} users spread out in {len(facility_ids)} facilities.')
	users = []
	emails = create_unique_items(len(facility_ids), email_address)
	full_names = create_unique_items(len(facility_ids), full_name)
	for facility_id, email_, name in zip(facility_ids, emails, full_names):
		email = email_
		first_name, last_name = name.split(' ')
		invite = dict(email=email, role_id=2, facility_id=facility_id, parent_id=None, use_mfa=False, use_keypad=False, is_active=False)
		signup = dict(first_name=first_name, last_name=last_name, password='haha', phone_number=p_number())
		user = {
			'invite': invite,
			'signup': signup
		}
		users.append(user)
	data = {
		'users': users
	}
	resp = requests.post(new_fake_user_url, json=data, cookies=cookies, headers=headers)
	if resp.ok:
		users = resp.json()['data']
		return users
	resp.raise_for_status()

def credit_each_user(user_dicts, amount, headers):
	print('Crediting Users')
	data = []
	for user in user_dicts:
		credit_account_data["group_id"] = user['group_id']
		credit_account_data["customer_id"] = user['user_id']
		credit_account_data["tx_performed_by"] = user['user_id']
		credit_account_data["amount_paid"] = amount
		credit_account_data["reference"] = create_random_string(randrange(5, 11))
		credit_account_data["paystack_auth"] = create_random_string(randrange(5, 11))
		credit_account_data["meter_id"] = create_random_string(randrange(5, 11))
		credit_account_data['last_four'] = int(''.join([str(i) for i in sample(range(0, 10), randrange(4, 9))]))
		data.append(credit_account_data)

	account_credits = {
		'bulk_credit': data
	}
	resp = requests.post(bulk_credit_account_url, json=account_credits, cookies=cookies, headers=headers)
	if resp.ok:
		return resp.json()['data']
	resp.raise_for_status()

def create_prosumers(subnet_id, user_dicts, prosumer_types, prosumer_groups, prosumers, headers):
	print("Creating Prosumers")
	api_data = {
		'prosumers': []
	}
	names = create_unique_items(len(user_dicts), first_name)
	print("\n\t\tProsumer creation details")
	# print(f"\t\tProsumer count {len(list(prosumers))}")
	print(f"\t\tUser count {len(user_dicts)}")
	print(f"\t\tName count {len(names)}")

	for prosumer, user, name in zip(prosumers, user_dicts, names):
		prosumer_data = {}
		facility_id = user['facilities'][0]['facility_id']
		probe_names = list(set(prosumer['probe_names']))
		prosumer_data['prosumer_name'] = name
		prosumer_data['user_group_id'] = user['group_id']
		prosumer_data['facility_id'] = facility_id
		prosumer_data['device_id'] = prosumer['meter_id']
		prosumer_data['prosumer_url'] = prosumer['meter_url']
		prosumer_data['prosumer_type_id'] = get_one_prosumer_type(prosumer['source_type'], prosumer_types)['prosumer_type_id']
		prosumer_data['prosumer_group_id'] = get_one_prosumer_group(facility_id, prosumer_groups)['group_id']
		prosumer_data['subnet_ids'] = [subnet_id]
		prosumer_data['probe_names'] = probe_names
		api_data['prosumers'].append(prosumer_data)
	resp = requests.post(bulk_new_prosumers_url, json=api_data, cookies=cookies, headers=headers)
	data = resp.json()['data']
	# print(f'New prosumers = {data}')
	print(f"{[i['prosumer_type']['prosumer_class_name'] for i in data]}")
	print(f"Prosumer count {len(data)}")
	return data

def get_prosumer_types(headers):
	return requests.get(prosumer_types_url, cookies=cookies, headers=headers).json()['data']

def get_one_prosumer_type(prosumer_type_name, prosumer_types):
	for prosumer_type in prosumer_types:
		if prosumer_type_name == prosumer_type['prosumer_type_name']:
			# print(f"Prosumer type selected {prosumer_type['prosumer_type_name']}")
			return prosumer_type
	raise(Exception("Prosumer type not found."))
	# return choice(prosumer_types)

def create_prosumer_groups_per_facility(facilities, headers):
	print('Creating prosumer groups in all facilities.')
	facility_ids = [facility['facility_id'] for facility in facilities]
	api_data = {
		'prosumer_groups': []
	}
	names = create_unique_items(len(facility_ids), first_name)
	for facility_id, name in zip(facility_ids, names):
		prosumer_group_data = {}
		prosumer_group_data['name'] = name
		prosumer_group_data['facility_id'] = facility_id
		api_data['prosumer_groups'].append(prosumer_group_data)
	prosumer_groups = requests.post(bulk_new_prosumer_groups_url, json=api_data, cookies=cookies, headers=headers).json()['data']
	return prosumer_groups

def get_one_prosumer_group(facility_id, prosumer_groups):
	for group in prosumer_groups:
		if facility_id == group['facility_id']:
			return group
	return choice(prosumer_groups)	

# get_circuit_type_id()
# get_subnet_type_id()
# get_current_type_id()

def setup_subnet(facility_count=30):
	api_key, user_dict = get_api_key()
	headers = {'X-API-KEY': api_key}
	source_count = int(facility_count / 3)
	facilities = get_facilities(facility_count, headers)
	print(f'Got {len(facilities)} facilities.')

	subnet = create_subnet(facilities, cookies, headers)
	users = create_user_per_facility(facilities, headers)
	prosumer_groups = create_prosumer_groups_per_facility(facilities, headers)

	credit_each_user(users, 10000000, headers)
	prosumer_types = get_prosumer_types(headers)

	three_phase_generators = get_three_phase_generators(source_count, headers)
	three_phase_consumers = get_three_phase_consumers(source_count, headers)
	one_phase_consumers = get_one_phase_consumers(source_count, headers)

	from itertools import chain
	prosumers = chain(three_phase_generators, three_phase_consumers, one_phase_consumers)
	# prosumers = three_phase_generators
	subnet_id = subnet['subnet_id']
	new_prosumers = create_prosumers(subnet_id, users, prosumer_types, prosumer_groups, prosumers, headers)
	
	response = {}
	response['success'] = True
	response['message'] = f"subnet successfully setup."
	return response, 201

# setup_subnet()
