from simulator.producer.message_producer import begin_metering
from logger import logger

logger.info('Beginning electricity production.')
begin_metering(is_a_consumer=False)
