#!/bin/sh

source venv/bin/activate

export FLASK_APP=sim_api.py
exec gunicorn -b :5000 -k eventlet --access-logfile - --error-logfile - sim_api:app --timeout 2000
