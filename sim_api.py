# Test Server.
from pprint import pprint

from flask import Flask, redirect, render_template
from flask_restplus import Api, Resource, fields

from api.new_orders import new_fake_order
from api.subnet_setup import setup_subnet

app = Flask(__name__)
new_api = Api(app, version='1.0', doc='/doc/', title='Simulator API',
    description='Mock orders and subnet creation API',
)
ns = new_api.namespace('simulator-setup', description='Simulator setup operations')


@ns.route('/setup-subnet')
class SetupSubnet(Resource):
    def post(self):
        '''Create new subnet. Just make a request to this url to activate it.'''
        return setup_subnet()

@ns.route('/new-orders')
class CreateNewOrders(Resource):
    def post(self):
        '''Create new orders. Just make a request to this url to activate it.'''
        return new_fake_order()
