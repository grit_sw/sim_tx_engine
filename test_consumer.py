from confluent_kafka import Consumer, KafkaError


c = Consumer({
    'bootstrap.servers': 'dashboard.grit.systems',
    'group.id': 'mygroup',
    'auto.offset.reset': 'earliest'
})

c.subscribe(['test-topic-1234'])

while True:
    print('Waiting for message')
    msg = c.poll(5.0)

    if msg is None:
        continue
    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            continue
        else:
            print(msg.error())
            break

    print('Received message: {}'.format(msg.value().decode('utf-8')))

c.close()
